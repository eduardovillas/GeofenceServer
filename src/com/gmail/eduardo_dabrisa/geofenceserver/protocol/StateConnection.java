/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gmail.eduardo_dabrisa.geofenceserver.protocol;

/**
 *
 * @author eduardov
 */
public enum StateConnection {
        NOT_CONNECTED,
        HAND_SHAKE,
        WAIT_FOR_READ,
        WAIT_FOR_WRITE
}
