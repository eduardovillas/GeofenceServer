/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gmail.eduardo_dabrisa.geofenceserver.protocol;

import com.gmail.eduardo_dabrisa.geofenceprotocollibrary.util.GeofenceServicePackage;
import com.gmail.eduardo_dabrisa.geofenceprotocollibrary.util.Property;
import java.net.Socket;
import java.util.List;

/**
 *
 * @author eduardov
 */
public class GeofenceServerProtocol {

    private Socket socket;
    private StateConnection stateConnection;

    public GeofenceServerProtocol(Socket socket) {
        this.socket = socket;
        stateConnection = StateConnection.NOT_CONNECTED;
        if (socket != null && socket.isConnected() && !socket.isClosed()) {
            stateConnection = StateConnection.HAND_SHAKE;
        }
    }

    public Boolean isConnected() {
        if (socket == null
                || !socket.isConnected()
                || socket.isClosed()
                || stateConnection == StateConnection.NOT_CONNECTED) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public StateConnection getStateConnection() {
        return stateConnection;
    }

    void disconnect() {
        stateConnection = StateConnection.NOT_CONNECTED;
        try {
            socket.close();
        } catch (Exception e) {
        }
    }

    public Boolean handShake(GeofenceServicePackage geofenceServicePackage) {
        geofenceServicePackage.receive(socket);

        if (!isConnected()) {
            close();
        }
        
        List<Property> properties = geofenceServicePackage.getProperties();
        
        if (properties.size() != 3) {
            close();
            return Boolean.FALSE;
        }
        
        Property applicationID = properties.get(0);
        Property identificationMac = properties.get(1);
        Property greeting = properties.get(2);
        if (!applicationID.getName().equals(Property.GEOFENCE_APPLICATION_ID) ||
                !identificationMac.getName().equals(Property.GEOFENCE_IDENTIFICATION_MAC) || 
                !greeting.getName().equals(Property.STRING)) {
            close();
            return Boolean.FALSE;
        }
        
        String id = applicationID.getValue();
        String mac = identificationMac.getValue();
        String hello = greeting.getValue();
        
        if (!hello.equals(Property.HELLO)) {
            close();
            return Boolean.FALSE;
        }
        
        System.out.println("handshake ok");

        geofenceServicePackage.clear();
        geofenceServicePackage.appendProperty(applicationID.STRING, Property.HELLO);
        geofenceServicePackage.send(socket);
        if (!isConnected()) {
            close();
            return Boolean.FALSE;
        }
        
        return Boolean.TRUE;
    }

    private void close() {
        stateConnection = StateConnection.NOT_CONNECTED;
        try {
            socket.close();
        } catch (Exception e) {

        }
    }

}
