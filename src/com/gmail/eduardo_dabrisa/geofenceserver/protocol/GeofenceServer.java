/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gmail.eduardo_dabrisa.geofenceserver.protocol;

import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;
import com.gmail.eduardo_dabrisa.geofenceprotocollibrary.util.GeofenceServicePackage;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author eduardov
 */
public class GeofenceServer {

    private final String host;
    private final int port;
    private ServerSocket serverSocket;
    private Socket socket;
    private StateConnection stateConnection;

    public GeofenceServer(String host, int port) {
        this.host = host;
        this.port = port;
        stateConnection = StateConnection.NOT_CONNECTED;
        socket = null;
        serverSocket = null;
    }

    public void forEver() {
        serverSocket = null;
        while (true) {
            if (!createServerSocket()) {
                continue;
            }

            try {
                System.out.println("Servidor 192.168.1.55:" + port + " aguardando um cliente");
                socket = serverSocket.accept();
                System.out.println("Servidor conectado a um cliente");
            } catch (Exception e) {
                socket = null;
            }

            SocketCommunicatioThread socketCommunicatioThread
                    = new SocketCommunicatioThread(socket);
            socketCommunicatioThread.start();
        }
    }

    private Boolean createServerSocket() {
        System.out.println("createServerSocket method");
        if (serverSocket != null && !serverSocket.isClosed()) {
            return Boolean.TRUE;
        }

        try {

            serverSocket = new ServerSocket(port);

        } catch (Exception e) {
            e.printStackTrace();
            serverSocket = null;
        }
        try {
            Thread.sleep(10000);
        } catch (Exception e) {
        }

        return serverSocket != null;
    }

    private String convertString(byte[] buffer) {
        String value = "";
        for (int i = 0; buffer.length > i; ++i) {
            value += (char) buffer[i];
        }
        return value;
    }

    public class SocketCommunicatioThread implements Runnable {

        private Thread thread;
        private GeofenceServerProtocol geofenceServerProtocol;

        public SocketCommunicatioThread(Socket socket) {
            thread = new Thread(this);
            geofenceServerProtocol = new GeofenceServerProtocol(socket);
        }

        public void start() {
            thread.start();
        }

        @Override
        public void run() {
            while (true) {

                if (!geofenceServerProtocol.isConnected()) {
                    return;
                }

                try {
                    switch (geofenceServerProtocol.getStateConnection()) {
                        case NOT_CONNECTED:
                            geofenceServerProtocol.disconnect();
                            break;
                        case HAND_SHAKE:
                            GeofenceServicePackage geofenceServicePackage = new GeofenceServicePackage();
                            geofenceServerProtocol.handShake(geofenceServicePackage);
                            
                        case WAIT_FOR_READ:
                        case WAIT_FOR_WRITE:

                    }
                } catch (Exception e) {
                }

                OutputStream outputStrem;
                InputStream inputStream;
                try {
                    outputStrem = socket.getOutputStream();
                    inputStream = socket.getInputStream();
                } catch (Exception e) {
                    continue;
                }

                while (socket.isConnected()) {
                    byte buffer[] = new byte[1024];
                    try {
                        System.out.println("Aguardando Cliente enviar msg");
                        inputStream.read(buffer);
                        System.out.println(convertString(buffer));
                    } catch (Exception e) {
                        continue;
                    }
                }

            }
        }
    }

}
