/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gmail.eduardo_dabrisa.geofenceserver.main;

import com.gmail.eduardo_dabrisa.geofenceprotocollibrary.util.ConfigurationsService;
import com.gmail.eduardo_dabrisa.geofenceprotocollibrary.util.GeofenceServicePackage;
import java.net.ServerSocket;
import java.net.Socket;
import com.gmail.eduardo_dabrisa.geofenceserver.protocol.GeofenceServer;

/**
 *
 * @author eduardov
 */
public class ConnectionGeofenceServer {

    private ServerSocket server;
    private Socket socket;
    private GeofenceServicePackage geofenceServicePackage;
    private GeofenceServer geofenceServerProtocol;
    private ConfigurationsService configurationsService;
    public ConnectionGeofenceServer(ConfigurationsService configurationsService) {
        server = null;
        socket = null;
        this.configurationsService = configurationsService;
        geofenceServerProtocol = new GeofenceServer(configurationsService.getHost(), 
                configurationsService.getPort());
    }
    
    
    public void run() {
        geofenceServerProtocol.forEver();
    }
}
