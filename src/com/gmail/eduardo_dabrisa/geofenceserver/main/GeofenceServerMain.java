/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gmail.eduardo_dabrisa.geofenceserver.main;

import com.gmail.eduardo_dabrisa.geofenceprotocollibrary.util.ConfigurationsService;

/**
 *
 * @author eduardov
 */
public class GeofenceServerMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ConnectionGeofenceServer s = new ConnectionGeofenceServer(new ConfigurationsService());
        s.run();
    }
    
}
